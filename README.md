# Data Processing
### Service for Stock data collection and processing
###### Mateusz Szczepański 22.12.2018
#### Build localy:
```commandline
docker-compose -f local.yml build
```

#### Test localy 
To run the tests, check your test coverage, and generate an HTML coverage report::
````commandline
     coverage run -m pytest
     coverage html
     open htmlcov/index.html
````
#### Celery
##### This app comes with Celery.

To run a celery worker:

````commandline
    cd data_processing
    celery -A data_processing.taskapp worker -l info
````



##### Please note: 
######For Celery's import magic to work, it is important *where* the celery commands are run. If you are in the same folder with *manage.py*, you should be right.


## Docker Deployment
#### Prerequisites
- Docker 1.10+.
- Docker Compose 1.6+

#### Understanding the Docker Compose Setup
Before you begin, check out the `production.yml` file in the root of this project. Keep note of how it provides configuration for the following services:

- `django`: your application running behind `Gunicorn`;
- `postgres`: PostgreSQL database with the application’s relational data;
- `redis`: Redis instance for caching;
- `caddy`: Caddy web server with HTTPS on by default.
- `celeryworker` running a Celery worker process;
- `celerybeat` running a Celery beat process;
- `flower` running [Flower](https://github.com/mher/flower) 

#### Configuring the Stack
The majority of services above are configured through the use of environment variables. Just check out Configuring the Environment and you will know the drill.

To obtain logs and information about crashes in a production setup, make sure that you have access to an external Sentry instance (e.g. by creating an account with sentry.io), and set the SENTRY_DSN variable.

You will probably also need to setup the Mail backend, for example by adding a Mailgun API key and a Mailgun sender domain, otherwise, the account creation view will crash and result in a 500 error when the backend attempts to send an email to the account owner.


### Building and Running Production Stack
You will need to build the stack first. To do that, run:
```commandline
docker-compose -f production.yml build
```
Once this is ready, you can run it with:
```commandline
docker-compose -f production.yml up
```
To run the stack and detach the containers, run:
```commandline
docker-compose -f production.yml up -d
```
To run a migration, open up a second terminal and run:
```commandline
docker-compose -f production.yml run --rm django python manage.py migrate
```
To create a superuser, run:
```commandline
docker-compose -f production.yml run --rm django python manage.py createsuperuser
```
If you need a shell, run:
```commandline
docker-compose -f production.yml run --rm django python manage.py shell
```
To check the logs out, run:
```commandline
docker-compose -f production.yml logs
```
If you want to scale your application, run:
```commandline
docker-compose -f production.yml scale django=4
docker-compose -f production.yml scale celeryworker=2
```
